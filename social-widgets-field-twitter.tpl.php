<?php if (!empty($twitter_id)) : ?>
<script src="http://platform.twitter.com/widgets.js"></script>

<a class="twitter-timeline" 
  href="https://twitter.com/<?php print $twitter_name ?>" 
  data-widget-id="<?php print $twitter_id ?>"
  data-theme="<?php print $theme ?>"
  width="<?php print $width ?>"
  height="<?php print $height ?>">
  Tweets by @<?php print $twitter_name ?>
</a>

<?php endif; ?>